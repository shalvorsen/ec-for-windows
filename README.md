# ec for Windows 7/8

ec is an open source multi-threaded command line (cmd) tool used to easily compile [Emerald](http://www.emeraldprogramminglanguage.org/) programs. NB: Requires that you have installed the [Emerald Windows compiler](http://www.uio.no/studier/emner/matnat/ifi/INF5510/v13/installere.html).

## Installation:

Copy ec.exe into the your emerald «bin» directory (i.e.: C:\emerald\bin).

Make sure your emerald bin directory is listed in the Path Windows environment variable.

1. Go to computer properties Right click -> properties
2. Click Advanced system settings
3. Click [Environment Variables...]
4. Find/mark Path under System variables
5. Click [Edit...]
6. Add ;C:\emerald\bin.
7. Click [OK], and log out of Windows or reboot.

You are now able to call ec from any location within CMD.

## Usage

***Compile a single file***

    C:\Users\username\> ec kilroy.m
    Compiling kilroy.m

***Compile a multiple files (1 to n)***

    C:\Users\username\> ec kilroy.m timesynch.m hi-ho.m
    Compiling timesynch.m
    Compiling hi-ho.m

***Alternate compilation of a single file***

    C:\Users\username\> ec
    Command: kilroy.m
    Compiling kilroy.m
    Command: q

## MIT License

«ec for Windows 7/8» is open-source C++11 and the source code can be redeemed at the following git repository; [https://bitbucket.org/shalvorsen/ec-for-windows](https://bitbucket.org/shalvorsen/ec-for-windows). The Emerald compiler & source code is not and should never be distributed with this software as it is licensed under the GNU General Public Licence v2.

Copyright (C) 2013 Stig M. Halvorsen ([everythingismyfault.com](http://www.everythingismyfault.com))

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.