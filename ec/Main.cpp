/*
 * @file: Main.cpp
 * @version: 1.0.0 <11.03.2013>
 * @author: Stig M. Halvorsen <halsti@nith.no>
 *
 * @description: ec is an open source multi-threaded command
 *				 line (cmd) tool used to easily compile Emerald
 *				 programs.
 *				 
 *				 NB: Requires that you have installed the Emerald
 *					 Windows compiler.
 *				 
 *				 NB: Compile with C++11 standard (Visual Studio 2012)!
 */

// Include prototypes and globals
#include "Main.h"

/* Main program entry point, retrieves user input. */
int main(int argc, char* argv[])
{
	// Check number of arguments
	if (argc > 1)
	{
		// Calculate number of threads to start
		numThreadsTotal = argc - 1;

		// Create a compiling thread for each file
		for (int i = 1; i < argc; ++i)
		{
			thread compile(multiCompiler, argv[i]);
			compile.detach();
		}

		// Wait for all threads to complete.
		// Threads will be terminated without this.
		while (numThreadsTotal > numThreadsDone)
		{
			std::this_thread::yield();
		}
	}
	else
	{
		// No additional arguments, launch regular compiler
		system("emx c:/emerald/lib/compiler");
	}

	return EXIT_SUCCESS;
}

/* Used by threads, compiles a single Emerald file. */
void multiCompiler(const char* file)
{
	// Build the compiler string
	stringstream out;
	out <<  "echo " << file
		<< " | emx c:/emerald/lib/compiler";

	// Check for errors and fire off the compiler request
	if (out.good())
	{
		system(out.str().c_str());
	}

	++numThreadsDone;
}