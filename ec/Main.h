/*
 * @file: Main.h
 * @version: 1.0.0 <11.03.2013>
 * @author: Stig M. Halvorsen <halsti@nith.no>
 *
 * @description: ec is an open source multi-threaded command
 *				 line (cmd) tool used to easily compile Emerald
 *				 programs.
 *				 
 *				 NB: Requires that you have installed the Emerald
 *					 Windows compiler.
 *				 
 *				 NB: Compile with C++11 standard (Visual Studio 2012)!
 */

#ifndef MAIN_H
#define MAIN_H

// Include required files
#include <iostream>
#include <thread>
#include <atomic>
#include <sstream>

// Set namespaces to save some code
using std::stringstream;
using std::thread;
using std::atomic;

// Thread synchronizers
atomic<int> numThreadsDone = 0;
atomic<int> numThreadsTotal = 0;

/* Main program entry point, retrieves user input. */
int main(int argc, char* argv[]);

/* Used by threads, compiles a single Emerald file. */
void multiCompiler(const char* file);

#endif